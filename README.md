# GWT DominoKit Sample



## Getting started

Person service works with JDK 17  
Domino sample client set to JDK 11  
Maven to compile

## Backend
**person-service**
```shell
mvn clean spring-boot:run
```

## Frontend
**domino-sample-client**
```shell
mvn clean gwt:devmode
```
### Compile
```shell
mvn clean verify
ls -lh target/domino-sample-client-1.0.0-SNAPSHOT/personclient/*.cache.js
```
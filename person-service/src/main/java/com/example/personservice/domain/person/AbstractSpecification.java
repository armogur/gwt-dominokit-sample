package com.example.personservice.domain.person;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Preconditions;
import lombok.EqualsAndHashCode;
import org.hibernate.query.criteria.internal.path.RootImpl;
import org.hibernate.query.criteria.internal.path.SingularAttributePath;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.time.format.DateTimeParseException;
import java.util.*;
import java.util.stream.Collectors;

@EqualsAndHashCode
public class AbstractSpecification<T> implements Specification<T> {

    private final DbFilter dbFilter;
    private final ObjectMapper objectMapper;

    public AbstractSpecification(DbFilter dbFilter) {
        this.dbFilter = dbFilter;
        objectMapper = new ObjectMapper();
    }

    @Override
    public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
        Path path = getJoin(root);
        Class<?> javaType = path.getJavaType();
        query.distinct(true);

        switch (dbFilter.getOperation()) {
            case IN:
                return createClause(root);
            case LIKE:
                return builder.like(builder.lower(path.as(String.class)),
                        "%" + dbFilter.getFilterValue().toLowerCase() + "%");
            case LT:
                return builder.lessThan(path, createTypeSafeField(javaType, dbFilter.getFilterValue()));
            case LE:
                return builder.lessThanOrEqualTo(path, createTypeSafeField(javaType, dbFilter.getFilterValue()));
            case GT:
                return builder.greaterThan(path, createTypeSafeField(javaType, dbFilter.getFilterValue()));
            case GE:
                return builder.greaterThanOrEqualTo(path, createTypeSafeField(javaType, dbFilter.getFilterValue()));
            case EQ:
                if (javaType.isAssignableFrom(Date.class)) {
                    try {
                        MyDate myDate = objectMapper.readValue(dbFilter.getFilterValue(), MyDate.class);
                        List<Predicate> predicates = new ArrayList<>();
                        if (myDate.getYear() != null) {
                            predicates.add(builder.equal(builder.function("YEAR", Integer.class, path), myDate.getYear()));
                        }
                        if (myDate.getMonth() != null) {
                            predicates.add(builder.equal(builder.function("MONTH", Integer.class, path), myDate.getMonth()));
                        }
                        if (myDate.getDay() != null) {
                            predicates.add(builder.equal(builder.function("DAY", Integer.class, path), myDate.getDay()));
                        }
                        return builder.and(predicates.toArray(new Predicate[0]));
                    } catch (JsonProcessingException e) {
                        throw new IllegalArgumentException(String.format("Invalid MyDate %s", dbFilter.getFilterValue()));
                    }
                } else {
                    return builder.equal(path, createTypeSafeField(javaType, dbFilter.getFilterValue()));
                }
            default:
                throw new IllegalArgumentException("No such filter operation: " + dbFilter.getOperation());
        }
    }

    private Path<T> getJoin(From<T, T> root) {
        String[] pathSplit = dbFilter.getFieldName().split("[.]");
        Preconditions.checkArgument(pathSplit.length > 0, "Empty Field name");

        String lastPathName;
        if (pathSplit.length == 1) {
            lastPathName = pathSplit[0];
        } else {
            root = root.join(pathSplit[0]);
            for (int i = 1; i < pathSplit.length - 1; i++) {
                root = root.join(pathSplit[i]);
            }
            lastPathName = pathSplit[pathSplit.length - 1];
        }
        return getLastPathElement(root, lastPathName);
    }

    private Path<T> getLastPathElement(Path<T> path, String attributeName) {
        Path<T> result;
        if (path.get(attributeName) instanceof SingularAttributePath) {
            result = path.get(attributeName);
        } else if (path instanceof RootImpl) {
            result = ((RootImpl<T>) path).join(attributeName);
        } else {
            result = ((Join<T, T>) path).join(attributeName);
        }
        return result;
    }

    protected Predicate createClause(Root<T> root) {
        String[] elements = dbFilter.getFilterValue().split(",");
        Join<Object, Object> join = root.join(dbFilter.getFieldName());
        return join.in(Arrays.stream(elements).map(s -> createTypeSafeField(join.getJavaType(), s))
                .collect(Collectors.toSet()));
    }

    protected Comparable createTypeSafeField(Class fieldType, String fieldValue) {
        Comparable result = null;
        try {
            if (fieldType.equals(ZonedDateTime.class)) {
                result = ZonedDateTime.parse(fieldValue);
            } else if (fieldType.equals(LocalDate.class)) {
                result = LocalDate.parse(fieldValue);
            } else if (fieldType.equals(Long.class)) {
                result = Long.parseLong(fieldValue);
            } else if (fieldType.equals(Integer.class)) {
                result = Integer.parseInt(fieldValue);
            } else if (fieldType.equals(String.class)) {
                result = fieldValue;
            } else if (fieldType.equals(UUID.class)) {
                result = UUID.fromString(fieldValue);
            } else if (fieldType.equals(Boolean.TYPE)) {
                result = Boolean.valueOf(fieldValue);
            } else if (Enum.class.isAssignableFrom(fieldType)) {
                result = Arrays.stream(fieldType.getEnumConstants())
                        .map(Enum.class::cast)
                        .filter(e -> e.name().equals(fieldValue))
                        .findFirst()
                        .orElseThrow(() -> new IllegalArgumentException(
                                String.format("Unknown field value '%s' of type '%s'", fieldValue, fieldType)));
            }
        } catch (NumberFormatException | DateTimeParseException | IndexOutOfBoundsException ex) {
            throw new IllegalArgumentException("Cannot convert field value: " + fieldValue, ex);
        }
        return result;
    }
}

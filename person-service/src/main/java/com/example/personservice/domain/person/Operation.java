package com.example.personservice.domain.person;

public enum Operation {
    IN,
    LIKE,
    LT,
    LE,
    GT,
    GE,
    EQ
}

package com.example.personservice.domain.person;

import com.example.personservice.PersonMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/persons")
@RequiredArgsConstructor
public class PersonController {
    private final PersonRepository repository;
    private final PersonMapper personMapper;

    @CrossOrigin({"*"})
    @PostMapping("query")
    public ResponseEntity<Page<PersonDto>> getPersons(@RequestBody QueryDto<Person> queryDto) {
        Page<Person> personPage = repository.findAll(queryDto.getSpecification(PersonSpecification::new), queryDto.toPageable());
        Page<PersonDto> personDtoPage = personMapper.mapPage(personPage);
        return ResponseEntity.ok(personDtoPage);
    }
}

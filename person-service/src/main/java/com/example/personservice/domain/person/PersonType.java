package com.example.personservice.domain.person;

public enum PersonType {
    COOL, BORING, DANGEROUS
}

package com.example.personservice.domain.person;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Getter
@EqualsAndHashCode
public class DbFilter implements Serializable {
    private final String fieldName;
    private final Operation operation;
    private final String filterValue;
    private final boolean negate;

    public DbFilter(@NotNull @NonNull String fieldName, @NotNull @NonNull Operation operation,
                    @NotNull @NonNull String filterValue, boolean negate) {
        this.fieldName = fieldName;
        this.operation = operation;
        this.filterValue = filterValue;
        this.negate = negate;
    }
}

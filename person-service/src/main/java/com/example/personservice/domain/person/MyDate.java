package com.example.personservice.domain.person;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class MyDate {
    private final Integer year;
    private final Integer month;
    private final Integer day;

    @JsonCreator
    public MyDate(@JsonProperty("year") Integer year, @JsonProperty("month") Integer month,
                  @JsonProperty("day") Integer day) {
        this.year = year;
        this.month = month;
        this.day = day;
    }
}

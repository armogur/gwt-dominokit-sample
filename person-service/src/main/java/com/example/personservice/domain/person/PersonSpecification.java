package com.example.personservice.domain.person;

public class PersonSpecification extends AbstractSpecification<Person> {

    public PersonSpecification(DbFilter dbFilter) {
        super(dbFilter);
    }
}

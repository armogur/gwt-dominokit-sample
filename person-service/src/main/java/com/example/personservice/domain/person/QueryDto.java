package com.example.personservice.domain.person;

import lombok.Getter;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;

@Getter
public class QueryDto<E> {
    public final int pageSize;
    public final int pageNumber;
    public final String propertyName;
    public final Sort.Direction sortDirection;
    private final List<DbFilter> filters;

    public QueryDto(int pageSize, int pageNumber, String propertyName, Sort.Direction sortDirection, List<DbFilter> filters) {
        this.pageSize = pageSize;
        this.pageNumber = pageNumber;
        this.propertyName = propertyName;
        this.sortDirection = sortDirection;
        this.filters = null == filters ? new ArrayList<>() : filters;
    }

    public Pageable toPageable() {
        Sort sort = Optional.ofNullable(propertyName)
                .filter(Predicate.not(String::isEmpty))
                .map(property -> Sort.by(sortDirection, propertyName))
                .orElse(Sort.unsorted());
        return PageRequest.of(pageNumber, pageSize, sort);
    }

    public Specification<E> getSpecification(Function<DbFilter, Specification<E>> function) {
        return filters.stream()
                .map(function)
                .reduce(Specification::and)
                .orElse(null);
    }
}

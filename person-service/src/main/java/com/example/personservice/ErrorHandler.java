package com.example.personservice;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Map;

@ControllerAdvice
public class ErrorHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(IllegalArgumentException.class)
    protected ResponseEntity<Object> handleIoException(IllegalArgumentException ex) {
        return createResponseContent(HttpStatus.PRECONDITION_FAILED, ex);
    }

    private ResponseEntity<Object> createResponseContent(HttpStatus status, Exception ex) {
        return new ResponseEntity<>(Map.of("status", status, "errors", ex.getMessage()), null, status);
    }
}

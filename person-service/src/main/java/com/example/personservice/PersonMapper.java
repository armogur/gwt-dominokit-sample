package com.example.personservice;

import com.example.personservice.domain.person.Person;
import com.example.personservice.domain.person.PersonDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PersonMapper {
    public Page<PersonDto> mapPage(Page<Person> pageOfEntities) {
        List<PersonDto> personDtoList = pageOfEntities.getContent()
                .stream()
                .map(person -> new PersonDto(person.getId(), person.getName(), person.getDate(), person.getPersonType()))
                .toList();
        return new PageImpl<>(personDtoList, pageOfEntities.getPageable(), pageOfEntities.getTotalElements());
    }
}

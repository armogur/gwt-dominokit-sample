package com.example.personservice;

import com.example.personservice.domain.person.Person;
import com.example.personservice.domain.person.PersonRepository;
import com.example.personservice.domain.person.PersonType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.Random;
import java.util.UUID;

@SpringBootApplication
public class PersonServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(PersonServiceApplication.class, args);
    }

    @Autowired
    PersonRepository repository;


    @PostConstruct
    void initDb() {
        String[] names = {"Steve", "Nick", "Flerken", "Groot", "Quill", "Drax", "Gamorra", "Thanos",
                "Fury", "Thor"};
        for (int i = 0; i < 111; i++) {
            String name = names[new Random().nextInt(names.length)];
            repository.save(new Person(UUID.randomUUID(), name, Date.from(
                    LocalDate.now().minusDays(i).atStartOfDay().toInstant(ZoneOffset.ofHours(1))),
                    PersonType.values()[i % PersonType.values().length]));
        }
    }

}

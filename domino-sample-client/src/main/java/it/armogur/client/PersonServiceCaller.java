package it.armogur.client;

import org.dominokit.rest.shared.request.FailedResponseBean;

import java.util.function.Consumer;

public class PersonServiceCaller implements LoadHandlers<PersonDto> {
    @Override
    public void load(QueryDto queryDto, Consumer<PageDto<PersonDto>> successConsumer, Consumer<FailedResponseBean> failConsumer) {
        PersonClientFactory.INSTANCE.getPersons(queryDto)
                .onSuccess(successConsumer::accept)
                .onFailed(failConsumer::accept).send();
    }
}

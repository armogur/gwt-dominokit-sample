package it.armogur.client;

import org.dominokit.domino.ui.datatable.plugins.SortDirection;

import javax.ws.rs.FormParam;
import java.util.List;

public class QueryDto {
    @FormParam("pageSize")
    public final int pageSize;
    @FormParam("pageNumber")
    public final int pageNumber;
    @FormParam("propertyName")
    public final String propertyName;
    @FormParam("sortDirection")
    public final SortDirection sortDirection;
    @FormParam("searchProperties")
    public final List<FilterDto> filters;

    public QueryDto(int pageSize, int pageNumber, String propertyName, SortDirection sortDirection,
                    List<FilterDto> filters) {
        this.pageSize = pageSize;
        this.pageNumber = pageNumber;
        this.propertyName = propertyName;
        this.sortDirection = sortDirection;
        this.filters = filters;
    }
}

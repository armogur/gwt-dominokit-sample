package it.armogur.client;

public class FilterDto {
    private final String fieldName;
    private final Operation operation;
    private final String filterValue;
    private final boolean negate;

    public FilterDto(String fieldName, Operation operation, String filterValue, boolean negate) {
        this.fieldName = fieldName;
        this.operation = operation;
        this.filterValue = filterValue;
        this.negate = negate;
    }

    public String getFieldName() {
        return fieldName;
    }

    public Operation getOperation() {
        return operation;
    }

    public String getFilterValue() {
        return filterValue;
    }

    public boolean isNegate() {
        return negate;
    }
}

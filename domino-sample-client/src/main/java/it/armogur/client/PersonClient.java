package it.armogur.client;

import org.dominokit.rest.shared.request.service.annotations.RequestFactory;

import javax.ws.rs.*;

@RequestFactory
public interface PersonClient {

	@POST
	@Path("/v1/persons/query")
	PageDto<PersonDto> getPersons(@BeanParam QueryDto queryDto);

}
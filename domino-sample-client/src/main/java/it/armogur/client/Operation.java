package it.armogur.client;

public enum Operation {
    IN,
    LIKE,
    LT,
    LE,
    GT,
    GE,
    EQ
}

package it.armogur.client;

import org.dominokit.domino.ui.datatable.events.SearchEvent;
import org.dominokit.domino.ui.datatable.events.SortEvent;
import org.dominokit.domino.ui.datatable.events.TableEvent;
import org.dominokit.domino.ui.datatable.events.TablePageChangeEvent;
import org.dominokit.domino.ui.datatable.model.Filter;
import org.dominokit.domino.ui.datatable.model.FilterTypes;
import org.dominokit.domino.ui.datatable.plugins.SortDirection;
import org.dominokit.domino.ui.datatable.store.DataChangedEvent;
import org.dominokit.domino.ui.datatable.store.DataStore;
import org.dominokit.domino.ui.datatable.store.StoreDataChangeListener;
import org.dominokit.domino.ui.pagination.HasPagination;

import java.util.*;
import java.util.logging.Logger;

import static org.dominokit.domino.ui.datatable.events.SearchEvent.SEARCH_EVENT;
import static org.dominokit.domino.ui.datatable.events.SortEvent.SORT_EVENT;

public class RemoteDataStore<T> implements DataStore<T> {
    private static Logger logger = Logger.getLogger(RemoteDataStore.class.getName());
    private List<StoreDataChangeListener<T>> listeners = new ArrayList<>();
    private final HasPagination pagination;
    private final LoadHandlers<T> loadHandlers;
    private String propertyName;
    private SortDirection sortDirection;
    private int activePage = 0;

    private final List<FilterDto> searchProperties = new ArrayList<>();
    private List<T> items;

    public RemoteDataStore(LoadHandlers<T> loadHandlers, HasPagination pagination) {
        this.loadHandlers = loadHandlers;
        this.pagination = pagination;
    }

    @Override
    public void onDataChanged(StoreDataChangeListener<T> dataChangeListener) {
        listeners.add(dataChangeListener);
    }

    @Override
    public void removeDataChangeListener(StoreDataChangeListener<T> dataChangeListener) {
        listeners.remove(dataChangeListener);
    }

    @Override
    public void load() {
        QueryDto queryDto = new QueryDto(pagination.getPageSize(), activePage, propertyName, sortDirection,
                searchProperties);
        loadHandlers.load(queryDto, loadResult -> {
                    items = loadResult.getContent();

                    pagination.updatePagesByTotalCount((int) loadResult.getTotalElements(), loadResult.getSize());
                    pagination.gotoPage(loadResult.getNumber() + 1, true);

                    fireUpdate();
                }, failedResponse ->
                    logger.severe("Status code = " + failedResponse.getStatusCode()
                            + ", response = "+ failedResponse.getBody())
        );
    }

    @Override
    public void handleEvent(TableEvent event) {
        switch (event.getType()) {
            case TablePageChangeEvent.PAGINATION_EVENT:
                activePage = pagination.activePage() - 1;
                load();
                break;
            case SORT_EVENT:
                propertyName = ((SortEvent<?>) event).getColumnConfig().getName();
                sortDirection = ((SortEvent<?>) event).getSortDirection();
                load();
                break;
            case SEARCH_EVENT:
                searchProperties.clear();
                List<Filter> filters = ((SearchEvent) event).getFilters();
                this.activePage = 0;
                for (Filter filter : filters) {
                    if (!filter.getValues().isEmpty()) {
                        if (filter.getType().equals(FilterTypes.DATE)) {
                            searchProperties.add(new FilterDto(filter.getFieldName(), Operation.EQ,
                                    new MyDate(filter.getValues()).toString(), false));
                        } else if (filter.getType().equals(FilterTypes.ENUM)) {
                            searchProperties.add(new FilterDto(filter.getFieldName(), Operation.EQ,
                                    filter.getValues().get(0), false));
                        } else {
                            searchProperties.add(new FilterDto(filter.getFieldName(), Operation.LIKE,
                                    filter.getValues().get(0), false));
                        }
                    }
                }
                load();
                break;
        }
    }

    private void fireUpdate() {
        listeners.forEach(dataChangeListener -> dataChangeListener.onDataChanged(new DataChangedEvent<>(items, items.size())));
    }
}

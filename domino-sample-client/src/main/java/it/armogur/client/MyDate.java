package it.armogur.client;

import java.util.List;

public class MyDate {
    private final Integer year;
    private final Integer month;
    private final Integer day;

    public MyDate(List<String> values) {
        this.year = nullSafeParse(values.get(0));
        this.month = nullSafeParse(values.get(1));
        this.day = nullSafeParse(values.get(2));
    }

    private Integer nullSafeParse(String value) {
        return value == null || value.isEmpty() ? null : Integer.parseInt(value);
    }

    @Override
    public String toString() {
        return "{\"year\":" + year +
                ", \"month\":" + month +
                ", \"day\":" + day + '}';
    }
}

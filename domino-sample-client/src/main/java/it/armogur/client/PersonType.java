package it.armogur.client;

public enum PersonType {
    COOL("Cool guy"), BORING("Boring"), DANGEROUS("Lethal");

    private final String type;

    PersonType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    @Override
    public String toString() {
        return type;
    }
}

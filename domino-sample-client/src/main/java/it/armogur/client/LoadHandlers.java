package it.armogur.client;

import org.dominokit.rest.shared.request.FailedResponseBean;

import java.util.function.Consumer;

public interface LoadHandlers<T> {
    void load(QueryDto queryDto, Consumer<PageDto<T>> successConsumer, Consumer<FailedResponseBean> failConsumer);
}

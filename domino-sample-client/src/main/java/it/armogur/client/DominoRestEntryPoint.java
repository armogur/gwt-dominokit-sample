package it.armogur.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.i18n.client.DateTimeFormat;
import elemental2.dom.DomGlobal;
import it.armogur.client.filter.MyDateHeaderFilter;
import it.armogur.client.filter.MyEnumHeaderFilter;
import org.dominokit.domino.ui.datatable.ColumnConfig;
import org.dominokit.domino.ui.datatable.DataTable;
import org.dominokit.domino.ui.datatable.TableConfig;
import org.dominokit.domino.ui.datatable.plugins.ColumnHeaderFilterPlugin;
import org.dominokit.domino.ui.datatable.plugins.HeaderBarPlugin;
import org.dominokit.domino.ui.datatable.plugins.SimplePaginationPlugin;
import org.dominokit.domino.ui.datatable.plugins.SortPlugin;
import org.dominokit.domino.ui.datatable.plugins.filter.header.TextHeaderFilter;
import org.dominokit.domino.ui.grid.GridLayout;
import org.dominokit.domino.ui.utils.TextNode;
import org.dominokit.rest.DominoRestConfig;

import java.util.logging.Logger;

public class DominoRestEntryPoint implements EntryPoint {

    private static final Logger logger = Logger.getLogger(DominoRestEntryPoint.class.getName());
    static final DateTimeFormat DATE_TIME_FORMAT = DateTimeFormat.getFormat("yyyy MMMM dd HH:mm:ss");

    @Override
    public void onModuleLoad() {
        DominoRestConfig.initDefaults();
        DominoRestConfig.getInstance().setDefaultServiceRoot("http://localhost:9090/server");

        TableConfig<PersonDto> tableConfig = new TableConfig<>();
        tableConfig
                .addColumn(
                        new ColumnConfig<PersonDto>("id", "Id")
                                .asHeader()
                                .setCellRenderer(
                                        cell -> TextNode.of(cell.getTableRow().getRecord().getId() + "")))
                .addColumn(
                        new ColumnConfig<PersonDto>("name", "Name")
                                .sortable()
                                .setCellRenderer(
                                        cell -> TextNode.of(cell.getTableRow().getRecord().getName())
                                ))
                .addColumn(
                        new ColumnConfig<PersonDto>("date", "Date")
                                .sortable()
                                .setCellRenderer(
                                        cell -> TextNode.of(DATE_TIME_FORMAT.format(cell.getTableRow().getRecord().getDate()))
                                )
                )
                .addColumn(
                        new ColumnConfig<PersonDto>("personType", "Personality")
                                .sortable()
                                .setCellRenderer(
                                        cell -> TextNode.of(cell.getTableRow().getRecord().getPersonType().getType())
                                )
                );

        SimplePaginationPlugin<PersonDto> simplePaginationPlugin = new SimplePaginationPlugin<>(10);

        tableConfig.addPlugin(simplePaginationPlugin)
                .addPlugin(new SortPlugin<>())
                .addPlugin(new HeaderBarPlugin<>("Persons", "From the working department"))
                .addPlugin(
                        new ColumnHeaderFilterPlugin<PersonDto>()
                                .addHeaderFilter("name", new TextHeaderFilter<>())
                                .addHeaderFilter("date", new MyDateHeaderFilter<>())
                                .addHeaderFilter("personType", new MyEnumHeaderFilter<>(PersonType.values(), "All"))
                );

        RemoteDataStore<PersonDto> remoteDataStore = new RemoteDataStore<>(new PersonServiceCaller(),
                simplePaginationPlugin.getSimplePagination());
        DataTable<PersonDto> table = new DataTable<>(tableConfig, remoteDataStore);
        table.load();

        GridLayout gridLayout = GridLayout.create()
                .style()
                .setHeight("500px").get();
        gridLayout.getContentElement().appendChild(table);

        DomGlobal.document.body.appendChild(gridLayout.element());
        logger.info(this.getClass().getSimpleName() + " loaded");
    }
}

package it.armogur.client.filter;

import elemental2.dom.HTMLElement;
import org.dominokit.domino.ui.datatable.ColumnConfig;
import org.dominokit.domino.ui.datatable.model.*;
import org.dominokit.domino.ui.datatable.plugins.ColumnHeaderFilterPlugin;
import org.dominokit.domino.ui.forms.IntegerBox;
import org.dominokit.domino.ui.grid.flex.FlexLayout;
import org.dominokit.domino.ui.utils.HasChangeHandlers;

import java.util.Arrays;

public class MyDateHeaderFilter<T> implements ColumnHeaderFilterPlugin.HeaderFilter<T> {
    private final FlexLayout dateSearchBox;
    private final IntegerBox year;
    private final IntegerBox month;
    private final IntegerBox day;
    public MyDateHeaderFilter() {
        year = IntegerBox.create("Year").setMaxLength(4).setMinValue(1900).setMaxValue(2022).disableFormatting()
                .style().setMarginBottom("0px").setWidth("4em").get();
        month = IntegerBox.create("Month").setMaxLength(2).setMinValue(1).setMaxValue(12).disableFormatting()
                .style().setMarginBottom("0px").setMarginLeft("0.6em").setMarginRight("0.6em").setWidth("2.5em").get();
        day = IntegerBox.create("Day").setMaxLength(2).setMinValue(1).setMaxValue(31).disableFormatting()
                .style().setMarginBottom("0px").setWidth("2.5em").get();
        dateSearchBox = FlexLayout.create().appendChild(year).appendChild(month).appendChild(day);
    }

    @Override
    public void init(SearchContext<T> searchContext, ColumnConfig<T> columnConfig) {
        searchContext.addBeforeSearchHandler(
                context -> {
                    if (year.isEmpty() && month.isEmpty() && day.isEmpty()) {
                        searchContext.remove(columnConfig.getName(), Category.HEADER_FILTER);
                    } else {
                        searchContext.add(
                                new Filter(columnConfig.getName(), FilterTypes.DATE, Operator.like,
                                        Arrays.asList(year.getStringValue(), month.getStringValue(), day.getStringValue()),
                                        Category.HEADER_FILTER));
                    }
                });
        HasChangeHandlers.ChangeHandler<Integer> changeHandler = value -> {
            if (year.validate().isValid() && month.validate().isValid() && day.validate().isValid()) {
                searchContext.fireSearchEvent();
            }
        };
        year.addChangeHandler(changeHandler);
        month.addChangeHandler(changeHandler);
        day.addChangeHandler(changeHandler);
    }

    @Override
    public void clear() {
        year.pauseChangeHandlers();
        month.pauseChangeHandlers();
        day.pauseChangeHandlers();

        year.clear();
        month.clear();
        day.clear();

        year.resumeChangeHandlers();
        month.resumeChangeHandlers();
        day.resumeChangeHandlers();
    }

    @Override
    public HTMLElement element() {
        return dateSearchBox.element();
    }
}
